/*
 * Based on work Copyright 2015 Daniel Theophanes.
 * See https://github.com/kardianos/service/blob/master/example/runner/runner.go
 */

package main

import (
	"io"
	"log"
	"math/rand"
	"os"
	"os/exec"
	"syscall"
	"time"

	"github.com/kardianos/service"
)

/**
 * Service Management data
 */
type Serman struct {
	// Service wrapper
	Service service.Service
	// Configuration parameters
	Config *Config
	// Running command
	Cmd *exec.Cmd
	// Rotating log files
	AccessLog io.Writer
	ErrorLog  io.Writer
	// Termination flag
	Terminate bool
}

/**
 * Creates a new Service as specified by the config file
 */
func NewSerman(cfgFile string) (sm *Serman, err error) {
	// Read Config File
	if cfgFile == "" {
		cfgFile, err = ConfigPath()
		if err != nil {
			return nil, err
		}
	}
	conf, err := GetConfig(cfgFile)
	if err != nil {
		return nil, err
	}
	// Create service
	svcConfig := &service.Config{
		Name:        conf.Name,
		DisplayName: conf.DisplayName,
		Description: conf.Description,
	}
	sm = &Serman{
		Config:    conf,
		AccessLog: conf.LogFile("access.log"),
		ErrorLog:  conf.LogFile("error.log"),
		Terminate: false,
	}
	serv, err := service.New(sm, svcConfig)
	if err != nil {
		return nil, err
	}
	sm.Service = serv
	if err != nil {
		return nil, err
	}
	return sm, nil
}

/**
 * Sets the service logging to the given file name
 */
func (sm *Serman) Log(fileName string) {
	log.SetOutput(sm.Config.LogFile(fileName))
}

/**
 * Starts the service.
 */
func (sm *Serman) Start(s service.Service) (err error) {
	log.Print("Received Start signal")
	sm.Terminate = false
	rand.Seed(time.Now().Unix())
	go sm.Run(sm.Config.MaxRestarts)
	return nil
}

/**
 * Runs the program in a continuous loop.
 *
 * Before starting the program, this function checks for updates
 * in the "updated" folder.
 */
func (sm *Serman) Run(maxRestarts int) (err error) {
	defer func() {
		sm.Stop(sm.Service)
		sm.Cmd = nil
	}()
	throttle := 0
	maxSleep := 10 // sleep no longer than 10 seconds
	for {
		// Check the termination flag. TODO: maybe better accomplished
		// by using a channel.
		if sm.Terminate {
			log.Print("Terminating runner")
			return nil
		}
		// Throttle consecutive restarts
		if throttle > 0 {
			if throttle >= maxRestarts {
				log.Printf("Reached %d restarts, exiting runner", throttle)
				return err
			}
			log.Print("Throttling after error")
			time.Sleep(time.Duration(throttle%maxSleep) * time.Second)
		}
		// Update service
		err := Update(sm.Config)
		if err != nil {
			log.Printf("Error updating runner: %v", err)
			throttle += 1
			continue
		}
		// Run command
		sm.Cmd, err = NewCmd(sm.Config)
		if err != nil {
			log.Printf("Error reading command: %v", err)
			throttle += 1
			continue
		}
		sm.Cmd.Stdout = sm.AccessLog
		sm.Cmd.Stderr = sm.ErrorLog
		if sm.Terminate {
			return nil
		}
		log.Printf("Starting runner for service %s", sm.Config.DisplayName)
		err = sm.Cmd.Run()
		if err != nil {
			log.Printf("Error running: %v", err)
			throttle += 1
			continue
		}
		throttle = 0
	}
	return err
}

/**
 * Stops the service execution
 */
func (sm *Serman) Stop(s service.Service) (err error) {
	log.Print("Received Stop signal")
	sm.Terminate = true // do not restart the service in the runner
	if sm.Cmd == nil {
		log.Print("Runner not started (cmd == nil)")
		return nil
	}
	// Beware: in windows, ProcessState is nil even if the process
	// is running. Sh!t
	kill := true
	if sm.Cmd.ProcessState != nil && sm.Cmd.ProcessState.Exited() {
		log.Print("Process already exited")
		kill = false
	}
	// graceful shutdown with signals? in Windows? you dreamin'...
	if kill {
		if err = sm.Cmd.Process.Kill(); err != nil {
			log.Printf("Unable to stop command: %v", err)
		}
	}
	if service.Interactive() {
		os.Exit(0)
	}
	// sm.Service.Stop() // needed?
	return nil
}

/**
 * Replaces the program running in memory
 */
func (sm *Serman) Replace(cmdPath string, args []string) error {
	// cmdPath Must be an absolute path.
	// args[0] must be the program name.
	binary, err := exec.LookPath(cmdPath)
	if err != nil {
		return err
	}
	env := os.Environ()
	return syscall.Exec(binary, args, env)
}
