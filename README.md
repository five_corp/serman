Serman - Purpose-built smartBI Service Manager
==============================================

This is just a wrapper to run the remote smartBI application as a Windows service in the customer's server.

The application is intended to manager a service that follows these conventions:

  - All relevant service files are placed at folders below a common top level dir, e.g. %SERVICEDIR%. These folders are:
  
    - **%TOPLEVELDIR%\current**: Files belonging to the *current* version of the service.
    - **%TOPLEVELDIR%\obsolete**: Files belonging to the *previous* version of the service.
    - **%TOPLEVELDIR%\updated**: Files belonging to the *next* version of the service.
    - **%TOPLEVELDIR%\logs**: Folder for log files.
    - **%TOPLEVELDIR%\config**: Folder for configuration files.

  - When the service is booted, it first checks for the **updated** folder. If it exists,
  
    - The *obsolete* folder is removed.
    - The *current* folder is renamed to *obsolete*
    - The *updated* folder is renamed to *current*
    
  - Then, the application checks for a *service.yaml* file inside the *current* folder. This file must contain:
  
```
---
exec: "name of the executable to run in this folder"
args:
  - "array"
  - "of"
  - "arguments"
  - "for"
  - "the"
  - "exec"
env:
 - "environment=values"
 - "in=the"
 - "key=value"
 - "pair=format"
```

  - The specified executable is run at the %TOPLEVELDIR% folder, with the specified arguments and environment.
  
When the executable exits, it is restarted again in a continuous loop.

Configuration
-------------

The service manager has its own configuration file, **serman.yaml**, with the following parameters:

```
---
name:        "service name"
displayName: "service display name"
description: "service description"
dir:         "path to the TOPLEVELDIR"
logSize:     maximum size of the log file (number, mbytes)
logBackups:  number of backups of the log files to keep (number)
maxRestarts: maximum number of restarts in a row.
```

**maxRestarts** does not count successful executions of the command; it only counts against consecutive failed executions.

Logging
-------

The stdout of the executed program is saved to the file %TOPLEVELDIR%\logs\\**access.log**. The stderr of the program is saved to the file %TOPLEVELDIR%\logs\\**error.log**.

Those files are rotated whenever they reach the size specified by the *logSize* configuration parameter. Up to *logBackups* backups of the log files are stored.
