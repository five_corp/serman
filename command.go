/*
 * Based on work Copyright 2015 Daniel Theophanes.
 * See https://github.com/kardianos/service/blob/master/example/runner/runner.go
 */

package main

import (
	"io/ioutil"
	"os"
	"os/exec"
	"runtime"

	"gopkg.in/yaml.v2"
)

/**
 * Parameters required to run the wrapped program
 */
type Command struct {
	// Name of the executable at the "current" folder
	Exec string `yaml:"exec"`
	// Arguments for the executable
	Args []string `yaml:"args"`
	// Environment variables for the executable
	Env []string `yaml:"env"`
}

/**
 * Reads the config file
 */
func GetCmdConfig(path string) (cmd *Command, err error) {
	f, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	defer f.Close()
	data, err := ioutil.ReadAll(f)
	if err != nil {
		return nil, err
	}
	cmd = &Command{
		// sane defaults
		Exec: "main.exe",
	}
	err = yaml.Unmarshal(data, cmd)
	return cmd, err
}

// NewCmd creates a new Cmd configured according to the service.yaml file
func NewCmd(conf *Config) (cmd *exec.Cmd, err error) {
	cmdConfig, err := GetCmdConfig(conf.Current("service.yaml"))
	bootstrap := "bootstrap"
	if runtime.GOOS == "windows" {
		bootstrap = "bootstrap.exe"
	}
	if err != nil {
		// Try to run a bootstrap command (bootstrap.exe -update)
		cmdConfig = &Command{
			Exec: bootstrap,
			Args: []string{"-update"},
			Env:  []string{},
		}
		// return nil, err
	}
	fullExec, err := exec.LookPath(conf.Current(cmdConfig.Exec))
	if err != nil {
		return nil, err
	}
	cmd = exec.Command(fullExec, cmdConfig.Args...)
	cmd.Dir = conf.Dir
	cmd.Env = append(os.Environ(), cmdConfig.Env...)
	return cmd, nil
}
