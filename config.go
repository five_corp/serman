/*
 * Based on work Copyright 2015 Daniel Theophanes.
 * See https://github.com/kardianos/service/blob/master/example/runner/runner.go
 */

package main

import (
	"io"
	"io/ioutil"
	"os"
	"path/filepath"

	"gopkg.in/yaml.v2"

	"github.com/kardianos/osext"
	"gopkg.in/natefinch/lumberjack.v2"
)

/**
 * Configuration Parameters
 */
type Config struct {

	// Service Name
	Name string `yaml:"name"`
	// Service Display Name
	DisplayName string `yaml:"displayName"`
	// Service description
	Description string `yaml:"description"`
	// Number of times the service may be restarted in a row
	MaxRestarts int `yaml:"maxRestarts"`
	// Service folder. There may be several sub-folders under it:
	// - "current": The current version of the service.
	// - "updated": An updated version of the service.
	// - "obsolete": The previous version of the service.
	// - "config": Path to any configuration files.
	Dir string `yaml:"dir"`
	// Log folder
	LogDir string `yaml:"logDir"`
	// Maximum log file size (mbytes)
	LogSize int `yaml:"logSize"`
	// Maximum number of rotated log files
	LogBackups int `yaml:"logBackups"`
}

/**
 * Gets the default config file name based on the path
 * and the name of the executable
 */
func ConfigPath() (path string, err error) {
	fullPath, err := osext.Executable()
	if err != nil {
		return "", err
	}
	dir, execName := filepath.Split(fullPath)
	ext := filepath.Ext(execName)
	name := execName[:len(execName)-len(ext)]
	return filepath.Join(dir, name+".yaml"), nil
}

/**
 * Reads the config file
 */
func GetConfig(path string) (conf *Config, err error) {
	f, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	defer f.Close()
	data, err := ioutil.ReadAll(f)
	if err != nil {
		return nil, err
	}
	conf = &Config{
		// sane defaults
		Name:        "serman",
		DisplayName: "serman",
		Description: "serman",
		MaxRestarts: 5,
		Dir:         ".",
		LogDir:      "logs",
		LogSize:     10,
		LogBackups:  3,
	}
	err = yaml.Unmarshal(data, conf)
	return conf, err
}

/**
 * Creates a rotating log file under the "LogDir" folder
 */
func (conf *Config) LogFile(name string) io.Writer {
	return &lumberjack.Logger{
		Filename:   filepath.Join(conf.LogDir, name),
		MaxSize:    conf.LogSize,
		MaxBackups: conf.LogBackups,
		LocalTime:  false,
	}
}

/**
 * Returns the path to a folder under the top level dir
 */
func (conf *Config) Folder(folderName string) string {
	return filepath.Join(conf.Dir, folderName)
}

/**
 * Returns the full path to a file in the "current" dir
 */
func (conf *Config) Current(filename string) string {
	return filepath.Join(conf.Dir, "current", filename)
}
