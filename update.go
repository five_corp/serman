/*
 * Based on work Copyright 2015 Daniel Theophanes.
 * See https://github.com/kardianos/service/blob/master/example/runner/runner.go
 */

package main

import "os"

/**
 * Checks if a given directory exists
 */
func CheckDir(path string) (exist bool, err error) {
	info, err := os.Stat(path)
	if err != nil {
		// Stat returns a PathError object whit an error code in
		// the .Err field. Let's check if the error is "File not exist"
		if e, ok := err.(*os.PathError); ok && os.IsNotExist(e.Err) {
			err = nil
		}
		return false, err
	}
	return info.IsDir(), nil
}

/**
 * Updates the service folders.
 *
 * If "updated" folder exists, this function:
 *   - Removes the "obsolete" folder
 *   - Renames "current" to "obsolete"
 *   - Renames "updated" to "current"
 */
func Update(conf *Config) (err error) {
	// Check if update directory exists
	updated_path := conf.Folder("updated")
	updated, err := CheckDir(updated_path)
	if err != nil || !updated {
		return err
	}
	// If so, remove obsolete folder
	obsolete_path := conf.Folder("obsolete")
	obsolete, err := CheckDir(obsolete_path)
	if err == nil && obsolete {
		if err = os.RemoveAll(obsolete_path); err != nil {
			return err
		}
	}
	// Rename current folder to obsolete
	current_path := conf.Folder("current")
	current, err := CheckDir(current_path)
	if err == nil && current {
		if err = os.Rename(current_path, obsolete_path); err != nil {
			return err
		}
	}
	// And rename updated to current
	err = os.Rename(updated_path, current_path)
	if err != nil {
		// Try to rollback from "obsolete" to "current"
		if current {
			if back := os.Rename(obsolete_path, current_path); back != nil {
				err = back
			}
		}
	}
	return err
}
