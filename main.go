/*
 * Based on work Copyright 2015 Daniel Theophanes.
 * See https://github.com/kardianos/service/blob/master/example/runner/runner.go
 */

package main

import (
	"flag"
	"log"
	"os"

	"github.com/kardianos/service"
)

func main() {

	svcFlag := flag.String("service", "", "Control the system service.")
	flag.Parse()

	serman, err := NewSerman("")
	if err != nil {
		log.Fatal(err)
	}
	serman.Log("control.log")

	err = os.Chdir(serman.Config.Dir)
	if err != nil {
		log.Fatal(err)
	}

	if len(*svcFlag) != 0 {
		log.Printf("Received -service flag %s", *svcFlag)
		err := service.Control(serman.Service, *svcFlag)
		if err != nil {
			log.Printf("Valid actions: %q\n", service.ControlAction)
			log.Fatal(err)
		}
		return
	}

	log.Printf("Running interactively with args %v", os.Args)
	if err = serman.Service.Run(); err != nil {
		log.Fatal("Error running service:", err)
	}
}
